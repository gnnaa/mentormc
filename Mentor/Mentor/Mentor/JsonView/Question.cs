﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.JsonView;

namespace Mentor.Models
{
    public class Question : Response
    {
        public Question()
        {

        }
        public string QuestionText { get; set; }
        public string Answer1 { get; set; }
        public string Answer2 { get; set; }
        public string Answer3 { get; set; }
        public string Answer4 { get; set; }
        public int RigthAnswer { get; set; }

        public static List<Question> CreateFromString(string json)
        {
            List<Question> questions = new List<Question>();
            JArray a = JArray.Parse(json);
            var questArr = a.First;
            foreach (var item2 in questArr)
            {
                Question question = new Question();
                question.QuestionText = item2.ToString();
                questions.Add(question);
            }

            var ans1Arr = a.First.Next;
            int i = 0;
            foreach (var item2 in ans1Arr)
            {
                questions[i].Answer1 = item2.ToString();
                i++;
            }

            var ans2Arr = ans1Arr.Next;
            i = 0;
            foreach (var item2 in ans2Arr)
            {
                questions[i].Answer2 = item2.ToString();
                i++;
            }

            var ans3Arr = ans2Arr.Next;
            i = 0;
            foreach (var item2 in ans3Arr)
            {
                questions[i].Answer3 = item2.ToString();
                i++;
            }

            var ans4Arr = ans3Arr.Next;
            i = 0;
            foreach (var item2 in ans4Arr)
            {
                questions[i].Answer4 = item2.ToString();
                i++;
            }

            var raArr = ans4Arr.Next;
            i = 0;
            foreach (var item2 in raArr)
            {
                questions[i].RigthAnswer = item2.ToObject<int>();
                i++;
            }

            var IdArr = raArr.Next;
            i = 0;
            foreach (var item2 in IdArr)
            {
                questions[i].Id = item2.ToObject<int>();
                i++;
            }

            return questions;
        }
    }
}
