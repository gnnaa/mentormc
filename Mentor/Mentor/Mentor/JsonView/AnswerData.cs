﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mentor.JsonView
{
    public class AnswerData
    {
        public int       StudentId { get; set; }
        public int       TopicId   { get; set; }
        public int       Key       { get; set; } //0 - сдан, 1 - не сдан, 2 - по таймеру
        public List<int> GoodIds   { get; set; }
        public List<int> BadIds    { get; set; }
        public List<int> Answers   { get; set; }
        public string    WriteDT   { get; set; }
        public int       Rating    { get; set; }
    }
}
