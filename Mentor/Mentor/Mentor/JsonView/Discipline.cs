﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.IO;

namespace Mentor.JsonView
{
    public class Discipline : Response
    {
        public string Name { get; set; }                //title
        public string TeacherName { get; set; }         //name
    }
}
