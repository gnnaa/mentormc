﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Mentor.JsonView
{
    public class DiaryRecord
    {
        public string Date  { get; set; }
        public string Name  { get; set; }
        public int    Value { get; set; }
        public Color Result
        {
            get
            {
                if(Value == 1)
                {
                    var v = Color.Green;
                    return v;
                }
                else
                {
                    var v = Color.Red;
                    return v;
                }
            }
        }
    }

    public class Statictics
    {
        public int BARS    { get; set; }
        public int Visited { get; set; }
        public int Value   { get; set; }
    }

    public class TestResultResponce
    {
        public Statictics Statistics { get; set; }
        public List<DiaryRecord> DiaryRecords { get; set; }
    }

    public class ResultRequest
    {
        public int DisciplineId { get; set; }
        public int StudentId { get; set; }
    }
}
