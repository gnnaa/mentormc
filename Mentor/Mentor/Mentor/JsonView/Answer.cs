﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mentor.JsonView
{
    public struct Answer
    {
        public Answer(int answer_id, int stud_answer)
        {
            AI = answer_id;
            SA = stud_answer;
        }
        /// <summary>
        /// Answer Id
        /// </summary>
        public int AI { get; set; }
        /// <summary>
        /// Student answer
        /// </summary>
        public int SA { get; set; }
    }
}
