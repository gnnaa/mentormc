﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.JsonView;

namespace Mentor.JsonView
{
    public class TestBlank : Response
    {
        public int StartTime { get; set; }   //секунд с начала эпохи Unix
        public int Duration { get; set; }    //секунд на проведение теста
        public int IdTopic { get; set; }
        public int ErrorCount { get; set; }
        public string TestJSON { get; set; }
        public int NeedRate { get; set; }
    }
}
