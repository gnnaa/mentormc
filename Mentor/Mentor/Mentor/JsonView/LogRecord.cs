﻿//using Plugin.DeviceInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mentor.Models
{
    public class LogRecord
    {
        public string DateTime { get; set; }
        public string DeviceName { get; set; }
        public string Platform { get; set; }
        public string Version { get; set; }
        public string Message { get; set; }
        public string Stack { get; set; }

        public LogRecord()
        {
        }

        public LogRecord(string mess)
        {
            DateTime = System.DateTime.Now.ToString("yyyy-MM-dd");
            //DeviceName = CrossDeviceInfo.Current.DeviceName;
            //Platform = CrossDeviceInfo.Current.Platform.ToString();
            //Version = CrossDeviceInfo.Current.Version;
            Message = mess;
        }

        public LogRecord(string mess, string stack)
        {
            DateTime = System.DateTime.Now.ToString("yyyy-MM-dd");
            //DeviceName = CrossDeviceInfo.Current.DeviceName;
            //Platform = CrossDeviceInfo.Current.Platform.ToString();
            //Version = CrossDeviceInfo.Current.Version;
            Message = mess;
            Stack = stack;
        }

        public LogRecord(Exception e)
        {
            DateTime = System.DateTime.Now.ToString("yyyy-MM-dd");
            //DeviceName = CrossDeviceInfo.Current.DeviceName;
            //Platform = CrossDeviceInfo.Current.Platform.ToString();
            //Version = CrossDeviceInfo.Current.Version;
            Message = e.Message;
            Stack = e.StackTrace;
        }
    }
}
