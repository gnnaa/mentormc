﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mentor.JsonView
{
    public class AuthorizationRequest
    {
        public string Login { get; set; }
        public string Pass { get; set; }
    }
}
