﻿//using Android.OS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.JsonView;
using Mentor.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mentor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TestProcessPage : ContentPage
    {
        private RestClient _restClient = new RestClient();
        private TestBlank _testBlank;
        private Student _student;
        private List<Question> _questions = new List<Question>();
        private int _duration = 31;
        private bool _alive = true;
        private int _errorCount = 0;
        private int _errorCurrent = 0;

        private ImageButton _star0;
        private ImageButton _star1;
        private ImageButton _star2;
        private ImageButton _star3;
        private ImageButton _star4;

        /// <summary>
        /// Номер текущего вопроса (отображаемого на странице)
        /// </summary>
        private int _currQuestionNumber = -1;

        /// <summary>
        /// Номер выбранного ответа (последний выбранный вариант ответа)
        /// </summary>
        private int _currAnswerNumber = 0;
        private int[] _numbers = { 1, 2, 3, 4 };
        private Random _random;

        /// <summary>
        /// Id вопроса, на который студентом был дан верный ответ
        /// </summary>
        private List<int> _goodIds = new List<int>();

        /// <summary>
        /// ID вопроса, на который студентом был дан неверный ответ 
        /// </summary>
        private List<int> _badIds   = new List<int>();

        /// <summary>
        /// неверный вариант ответа, выбранный студентом
        /// </summary>
        private List<int> _studAnswerNums = new List<int>();


        public TestProcessPage()
        {
            InitializeComponent();
        }

        public TestProcessPage(TestBlank testBlank, Student student, Random random)
        {
            _questions.Clear();
            _testBlank = testBlank;
            _student = student;
            _questions = Question.CreateFromString(_testBlank.TestJSON);
            _duration = _testBlank.Duration;
            _errorCount = _testBlank.ErrorCount;
            _random = random;

            InitializeComponent();

            if (testBlank.NeedRate == 1)
            {
                StartRate();
            }
            else
            {
                StartTest();
            }
        }

        private void StartRate()
        {
            string on = "Mentor.Images.buttons.enable_star.png";
            string off = "Mentor.Images.buttons.disable_star.png";
            _star0 = new ImageButton(i0, on, off);
            _star1 = new ImageButton(i1, on, off);
            _star2 = new ImageButton(i2, on, off);
            _star3 = new ImageButton(i3, on, off);
            _star4 = new ImageButton(i4, on, off);

            ratingGrid.IsVisible = true;
            Device.StartTimer(TimeSpan.FromSeconds(1), OnRateTimerTick);
        }

        private void StartTest()
        {
            GetNextQuestion();
            Device.StartTimer(TimeSpan.FromSeconds(1), OnTimerTick);
            UpdateTimerLabel();
        }

        private void UpdateQuestionLabel()
        {
            labelCurQuestionNum.Text = String.Format("Вопрос: {0}/{1}", (_currQuestionNumber + 1), _questions.Count);
        }

        private void ShuffleArray()
        {
            for (int i = 3; i > 0; i--)
            {
                int swapIndex = _random.Next(i + 1);
                int temp = _numbers[i];
                _numbers[i] = _numbers[swapIndex];
                _numbers[swapIndex] = temp;
            }
        }

        private string GetAnswerTextById(int i)
        {
            string text = "";
            int num = _numbers[i];
            switch (num)
            {
                case 1:
                {
                    text = _questions[_currQuestionNumber].Answer1;
                    break;
                }
                case 2:
                {
                    text = _questions[_currQuestionNumber].Answer2;
                    break;
                }
                case 3:
                {
                    text = _questions[_currQuestionNumber].Answer3;
                    break;
                }
                case 4:
                {
                    text = _questions[_currQuestionNumber].Answer4;
                    break;
                }
            }
            return text;
        }

        private void GetNextQuestion()
        {
            _currAnswerNumber = 0;
            ClearColor();

            _currQuestionNumber++;
            if(_currQuestionNumber < _questions.Count)
            {
                ShuffleArray();
                UpdateQuestionLabel();
                labelQuestion.Text = _questions[_currQuestionNumber].QuestionText;
                buttonAnswer1.Text = GetAnswerTextById(0);
                buttonAnswer2.Text = GetAnswerTextById(1);
                buttonAnswer3.Text = GetAnswerTextById(2);
                buttonAnswer4.Text = GetAnswerTextById(3);
            }
            else
            {
                CheckTestResult();
            }
        }

        private AnswerData ComputeAnswerData()
        {
            AnswerData answerData = new AnswerData();
            answerData.TopicId = _testBlank.IdTopic;
            answerData.GoodIds = _goodIds;
            answerData.BadIds = _badIds;
            answerData.Answers = _studAnswerNums;
            answerData.WriteDT = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            answerData.StudentId = _student.Id;
            answerData.Rating = _ratingValue;
            return answerData;
        }

        private void CheckTestResult()
        {
            _alive = false;
            SetButtonsIsEnable(false);
            AnswerData answerData = ComputeAnswerData();           

            Device.BeginInvokeOnMainThread(async () =>
            {
                activityIndicatorGrid.IsVisible = true;
                labelTestResultMessage.Text = "";
                string resText = "Тест НЕ сдан.";                
                if (_errorCurrent <= _errorCount)
                {
                    resText = "Тест сдан!";
                    answerData.Key = 1;
                }
                else
                {
                    answerData.Key = 0;
                }

                labelLoadingStage.Text = "Запись в локальную БД";
                Synchronizer.WriteAnswerDataToLocal(answerData);
                labelTestResultMessage.Text = resText;
                labelLoadingStage.Text = "Синхронизация с \"облачной\" БД";
                Response response = await Synchronizer.Synchronize();
                if (response.Id > -1)
                {
                    await this.DisplayAlert("Результат", resText + " Данные успешно синхронизированы с \"облачной\" БД", "OK");
                    await Navigation.PopModalAsync(false);
                }
                else
                {
                    await this.DisplayAlert("Результат", resText +
                                            " Данные сохранены в локальной БД и " +
                                            "будут синхронизированы с \"облачной\" БД " +
                                            "при возобновлении доступа к сети интернет", "OK");
                    await Navigation.PopModalAsync(false);
                }
            });
        }

        private void CheckAnswerResult()
        {
            if(_currAnswerNumber > 0)
            {
                if( _numbers[_currAnswerNumber - 1] != _questions[_currQuestionNumber].RigthAnswer)
                {
                    _errorCurrent++;
                    _badIds.Add(_questions[_currQuestionNumber].Id);
                    _studAnswerNums.Add(_numbers[_currAnswerNumber - 1]);
                }
                else
                {
                    _goodIds.Add(_questions[_currQuestionNumber].Id);
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => 
            {
                var result = await this.DisplayAlert("Внимание!", "Вы уверены что хотите закрыть приложение?", "Да", "Нет");
                //if (result) { Process.KillProcess(Process.MyPid()); } // or anything else
            });

            return true;
        }

        /// <summary>
        /// Вызывается по истечении времени на прохождение тестирования
        /// </summary>
        /// <param name="obj"></param>
        private  bool OnTimerTick()
        {
            if (_alive == true)
            {
                _duration--;
                UpdateTimerLabel();

                if (_duration <= 0 && _alive == true)
                {
                    SetButtonsIsEnable(false);
                    _alive = false;                    

                    if (_currQuestionNumber < _questions.Count)
                    {
                        //считаем: на сколько вопросов студент не успел ответить
                        int unansweredCount = _questions.Count - _currQuestionNumber;
                        _errorCurrent += unansweredCount;
                    }

                    _badIds.Add(_questions[_currQuestionNumber].Id);
                    _studAnswerNums.Add(0);
                    CheckTestResult();
                }
            }       
            return _alive;
        }

        private void SetButtonsIsEnable(bool b)
        {
            buttonFinish.IsEnabled = b;
            buttonAnswer1.IsEnabled = b;
            buttonAnswer2.IsEnabled = b;
            buttonAnswer3.IsEnabled = b;
            buttonAnswer4.IsEnabled = b;
        }

        private string GetTimerString()
        {
            string res = "Время: " + TimeSpan.FromSeconds(_duration).ToString(@"mm\:ss");
            return res;
        }

        private string GetRateTimerString()
        {
            string res = "Время на оценку: " + TimeSpan.FromSeconds(_durationRate).ToString(@"ss");
            return res;
        }

        private void UpdateTimerLabel()
        {
            labelTimer.Text = GetTimerString();
        }

        private void UpdateRateTimerLabel()
        {
            labelRate.Text = GetRateTimerString();
        }

        private async void UpdateColor()
        {
            const int _animationTime = 80;
            ClearColor();
            switch (_currAnswerNumber)
            {
                
                case 1:
                    {
                        buttonAnswer1.BackgroundColor = Color.WhiteSmoke;
                        buttonAnswer1.TextColor = Color.CornflowerBlue;                        
                        await buttonAnswer1.ScaleTo(1.15, _animationTime); 
                        await buttonAnswer1.ScaleTo(1, _animationTime);
                        
                        break;
                    }
                case 2:
                    {
                        buttonAnswer2.BackgroundColor = Color.WhiteSmoke;
                        buttonAnswer2.TextColor = Color.CornflowerBlue;
                        await buttonAnswer2.ScaleTo(1.15, _animationTime);
                        await buttonAnswer2.ScaleTo(1, _animationTime);
                        break;
                    }
                case 3:
                    {
                        buttonAnswer3.BackgroundColor = Color.WhiteSmoke;
                        buttonAnswer3.TextColor = Color.CornflowerBlue;
                        await buttonAnswer3.ScaleTo(1.15, _animationTime);
                        await buttonAnswer3.ScaleTo(1, _animationTime);
                        break;
                    }
                case 4:
                    {
                        buttonAnswer4.BackgroundColor = Color.WhiteSmoke;
                        buttonAnswer4.TextColor = Color.CornflowerBlue;
                        await buttonAnswer4.ScaleTo(1.15, _animationTime);
                        await buttonAnswer4.ScaleTo(1, _animationTime);
                        break;
                    }
            }
        }

        private void ClearColor()
        {
            buttonAnswer1.BackgroundColor = buttonFinish.BackgroundColor;
            buttonAnswer1.TextColor = Color.White;
            buttonAnswer2.BackgroundColor = buttonFinish.BackgroundColor;
            buttonAnswer2.TextColor = Color.White;
            buttonAnswer3.BackgroundColor = buttonFinish.BackgroundColor;
            buttonAnswer3.TextColor = Color.White;
            buttonAnswer4.BackgroundColor = buttonFinish.BackgroundColor;
            buttonAnswer4.TextColor = Color.White;
        }

        private void buttonAnswer1_Clicked(object sender, EventArgs e)
        {
            _currAnswerNumber = 1;
            UpdateColor();
        }

        private void buttonAnswer2_Clicked(object sender, EventArgs e)
        {
            _currAnswerNumber = 2;
            UpdateColor();
        }

        private void buttonAnswer3_Clicked(object sender, EventArgs e)
        {
            _currAnswerNumber = 3;
            UpdateColor();
        }

        private void buttonAnswer4_Clicked(object sender, EventArgs e)
        {
            _currAnswerNumber = 4;
            UpdateColor();
        }

        private void buttonFinish_Clicked(object sender, EventArgs e)
        {
            if(_currAnswerNumber > 0)
            {
                CheckAnswerResult();
                GetNextQuestion();
            }
        }

        private bool _b = false;
        private void buttonRate_Clicked(object sender, EventArgs e)
        {
            buttonRate.IsEnabled = false;
            _aliveRate = false;
            ratingGrid.IsVisible = false;
            StartTest();
        }

        private bool _aliveRate = true;
        private int _durationRate = 15;

        /// <summary>
        /// Вызывается по истечении времени на прохождение тестирования
        /// </summary>
        /// <param name="obj"></param>
        private bool OnRateTimerTick()
        {
            if (_aliveRate == true)
            {
                _durationRate--;
                UpdateRateTimerLabel();

                if (_durationRate <= 0 && _aliveRate == true)
                {
                    buttonRate.IsEnabled = false;
                    _aliveRate = false;
                    ratingGrid.IsVisible = false;
                    StartTest();
                }
            }
            return _aliveRate;
        }

        private int _ratingValue = 0;
        private void TapGestureRecognizerI0_Tapped(object sender, EventArgs e)
        {
            ApplyRate(1);
        }
        private void TapGestureRecognizerI1_Tapped(object sender, EventArgs e)
        {
            ApplyRate(2);
        }
        private void TapGestureRecognizerI2_Tapped(object sender, EventArgs e)
        {
            ApplyRate(3);
        }
        private void TapGestureRecognizerI3_Tapped(object sender, EventArgs e)
        {
            ApplyRate(4);
        }
        private void TapGestureRecognizerI4_Tapped(object sender, EventArgs e)
        {
            ApplyRate(5);
        }

        private void ApplyRate(int r)
        {
            _ratingValue = r;
            if (buttonRate.IsEnabled == false) buttonRate.IsEnabled = true;
            RecolorizeStars();
        }

        private void RecolorizeStars()
        {
            switch (_ratingValue)
            {
                case 0:
                    {
                        _star0.IsOn = false;
                        _star1.IsOn = false;
                        _star2.IsOn = false;
                        _star3.IsOn = false;
                        _star4.IsOn = false;
                        break;
                    }
                case 1:
                    {
                        _star0.IsOn = true;
                        _star1.IsOn = false;
                        _star2.IsOn = false;
                        _star3.IsOn = false;
                        _star4.IsOn = false;
                        break;
                    }
                case 2:
                    {
                        _star0.IsOn = true;
                        _star1.IsOn = true;
                        _star2.IsOn = false;
                        _star3.IsOn = false;
                        _star4.IsOn = false;
                        break;
                    }
                case 3:
                    {
                        _star0.IsOn = true;
                        _star1.IsOn = true;
                        _star2.IsOn = true;
                        _star3.IsOn = false;
                        _star4.IsOn = false;
                        break;
                    }
                case 4:
                    {
                        _star0.IsOn = true;
                        _star1.IsOn = true;
                        _star2.IsOn = true;
                        _star3.IsOn = true;
                        _star4.IsOn = false;
                        break;
                    }
                case 5:
                    {
                        _star0.IsOn = true;
                        _star1.IsOn = true;
                        _star2.IsOn = true;
                        _star3.IsOn = true;
                        _star4.IsOn = true;
                        break;
                    }
            }
        }
    }
}