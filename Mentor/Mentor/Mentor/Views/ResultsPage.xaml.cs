﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Mentor.JsonView;
using Mentor.Models;
using System.Collections.ObjectModel;

namespace Mentor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResultsPage : ContentPage
    {
        List<Discipline> _disciplineList;
        Student stud;
        RestClient _restClient;
        ObservableCollection<DiaryRecord> _resultsList = new ObservableCollection<DiaryRecord>();

        public ResultsPage()
        {
            InitializeComponent();
        }

        public ResultsPage(Student _student)
        {
            _restClient = new RestClient();

            _disciplineList = new List<Discipline>();

            stud = _student;
            
            InitializeComponent();

            listView.ItemsSource = _resultsList;

            disciplineComboBox.IsVisible = false;
            //button_showDiary.IsVisible = false;
            //button_showDiary.IsEnabled = false;

            this.Appearing += ResultsPage_Appearing;
        }

        private async void ComboBoxIndexChanged(object sernder, EventArgs e)
        {
            if (_disciplineList != null && disciplineComboBox.SelectedIndex >= 0)
            {
                activityIndicatorGrid.IsVisible = true;
                ClearPage();

                ResultRequest request = new ResultRequest();
                request.DisciplineId = _disciplineList.ElementAt(disciplineComboBox.SelectedIndex).Id;
                request.StudentId = stud.Id;

                TestResultResponce resultResp = await _restClient.GetTestResults(request);
                if (resultResp != null &&
                    resultResp.DiaryRecords != null &&
                    resultResp.Statistics != null &&
                    resultResp.DiaryRecords.Count > 0
                    )
                {
                    labelBARS.Text = "БАРС: " + resultResp.Statistics.BARS.ToString();
                    labelVisited.Text = "Посещено: " + resultResp.Statistics.Visited.ToString() + "%";
                    labelValue.Text = "Сдано: " + resultResp.Statistics.Value.ToString() + "%";
                    AddToObservableCollection(resultResp.DiaryRecords, _resultsList);
                }
                else
                {
                    await DisplayAlert("Результаты", "Записи отсутствуют", "OK");
                }
                activityIndicatorGrid.IsVisible = false;
            }
        }

        private void ClearPage()
        {
            if(_resultsList != null)
            {
                _resultsList.Clear();
            }
            

            labelBARS.Text = "";
            labelVisited.Text = "";
            labelValue.Text = "";
        }

        private async void ResultsPage_Appearing(object sender, EventArgs e)
        {
            activityIndicatorGrid.IsVisible = true;
            disciplineComboBox.IsVisible = false;

            if(_disciplineList != null)
            {
                _disciplineList.Clear();
            }
            
            disciplineComboBox.Items.Clear();
            ClearPage();

            _disciplineList = await _restClient.GetDiscipline(stud);

            if ((_disciplineList != null) && (_disciplineList.Count != 0))
            {
                for (int i = 0; i < _disciplineList.Count; i++)
                {
                    disciplineComboBox.Items.Add(_disciplineList[i].Name + " (" + _disciplineList[i].TeacherName + ")");
                }
            }
            else
            {
                disciplineComboBox.Title = "Записи не найдены";
            }
            activityIndicatorGrid.IsVisible = false;
            disciplineComboBox.IsVisible = true;
        }

        private void AddToObservableCollection(  IEnumerable<DiaryRecord>         source, 
                                                ObservableCollection<DiaryRecord> target )
        {
            foreach (var v in source)
            {
                target.Add(v);
            }
        }
    }
}