﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Mentor.JsonView;
using Mentor.Views;

namespace Mentor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]    
    public partial class SettingsPage : ContentPage
    {
        Student _student;
        
        public SettingsPage()
        {
            InitializeComponent();

        }

        public SettingsPage(Student student)
        {
            InitializeComponent();
            _student = student;
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            //Process.KillProcess(Process.MyPid());
        }

        private void ChangePassBtn_Clicked(object sender, EventArgs e)
        {
            var profilePage = new NavigationPage(new ProfilePage(_student));
            //NavigationPage.SetHasBackButton(profilePage, true);
            Navigation.PushAsync(profilePage);
        }

        private void FeedbackBtn_Clicked(object sender, EventArgs e)
        {

        }
    }
}