﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.JsonView;
using Mentor.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mentor.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        private Student     _student;
        private EntryCell   _oldPass;
        private EntryCell   _newPass;
        private EntryCell   _newPassAgain;
        private ViewCell    _buttons;

        public ProfilePage()
        {
            InitializeComponent();

            this.Title = "Профиль";
        }

        public ProfilePage(Student student)
        {
            InitializeComponent();

            _student = student;

            entryCellFio.Text = _student.Name;
            entryCellLogin.Text = _student.Login;
            entryCellEmail.Text = _student.Email;

            this.Title = "Профиль";

            _oldPass        = new EntryCell { Placeholder = "Введите старый пароль" };
            _newPass        = new EntryCell { Placeholder = "Введите новый пароль" };
            _newPassAgain   = new EntryCell { Placeholder = "Повторите новый пароль" };
            _buttons        = CreateButtonsViewCell();
        }

        private void SwitchCell_OnChanged(object sender, ToggledEventArgs e)
        {
            if(switchCell.On == true)
            {
                SetIsEnableControls(true);

                tableSection.Add(_oldPass);
                tableSection.Add(_newPass);
                tableSection.Add(_newPassAgain);
                tableSection.Add(_buttons);
            }
            else
            {
                SetIsEnableControls(false);

                entryCellLogin.Text = _student.Login;
                entryCellEmail.Text = _student.Email;
                _oldPass.Text = "";
                _newPass.Text = "";
                _newPassAgain.Text = "";

                tableSection.Remove(_oldPass);
                tableSection.Remove(_newPass);
                tableSection.Remove(_newPassAgain);
                tableSection.Remove(_buttons);
            }
        }

        private void SetIsEnableControls(bool b)
        {
            entryCellLogin.IsEnabled = b;
            entryCellEmail.IsEnabled = b;
        }

        private ViewCell CreateButtonsViewCell()
        {
            Grid grid = new Grid
            {
                ColumnDefinitions =
                {
                    new ColumnDefinition(), 
                    new ColumnDefinition()
                }
            };

            Button ok       = new Button { Text = "Сохранить" };
            ok.Clicked += OnSaveProfile;

            Button cancel   = new Button { Text = "Отменить" };
            cancel.Clicked += OnCancelProfile;

            grid.Children.Add(cancel, 0, 0);
            grid.Children.Add(ok, 1, 0);

            ViewCell viewCell = new ViewCell();
            viewCell.View = grid;
            return viewCell;
        }

        private void OnCancelProfile(object sender, EventArgs e)
        {
            switchCell.On = false;
        }

        private async void OnSaveProfile(object sender, EventArgs e)
        {
            try
            {
                bool canWritePass  = CanWritePass();
                bool canWriteLogin = CanWriteLogin();
                bool canWriteEmail = CanWriteEmail();

                Student student = new Student();
                student.Set(_student);

                if (canWriteLogin == true)
                {
                    student.Login = entryCellLogin.Text;
                }
                if (canWriteEmail == true)
                {
                    student.Email = entryCellEmail.Text;
                }
                if (canWritePass == true)
                {
                    student.Pass = _newPass.Text;
                }

                if(student.Login != _student.Login ||
                   student.Email != _student.Email ||
                   student.Pass  != _student.Pass    )
                {
                    activityIndicatorGrid.IsVisible = true;
                    Response response = await RestClient.SetNewProfile(student);
                    if (response.Id > -1)
                    {
                        await DisplayAlert("Профиль", "Изменения приняты!", "OK");

                        if (student.Login != _student.Login ||
                             student.Pass != _student.Pass)
                        {
                            bool result = await this.DisplayAlert("Автоподстановка", "Сохранить новые данные авторизации для автоподстановки?", "Да", "Нет");
                            if (result)
                            {
                                AuthorizationRequest request = new AuthorizationRequest();
                                request.Login = student.Login;
                                request.Pass = student.Pass;
                                AutorizationSubstitute.WriteAutorizationData(request);
                            }
                        }

                        _student.Set(student);
                        switchCell.On = false;
                    }
                    else
                    {
                        await DisplayAlert("Профиль", response.Message, "OK");
                    }
                    activityIndicatorGrid.IsVisible = false;
                }                
            }
            catch(Exception ex)
            {
                await DisplayAlert("Профиль", ex.Message, "OK");
            }
        }

        private bool CanWriteLogin()
        {
            bool canWrite = false;
            string login = entryCellLogin.Text;
            if(login != null && login != _student.Login)
            {
                if (login.Length < 4)
                {
                    throw new Exception("Длина логина не может быть меньше 4 символов!");
                }
                canWrite = true;
            }

            return canWrite;
        }

        private bool CanWriteEmail()
        {
            bool canWrite = false;
            string email = entryCellEmail.Text;
            if (email != null && email != _student.Email)
            {
                if (email.Length < 6)
                {
                    throw new Exception("Длина адреса электронной почты не может быть меньше 6 символов!");
                }
                canWrite = true;
            }

            return canWrite;
        }

        private bool CanWritePass()
        {
            bool canWrite = false;
            string pass = _newPass.Text;
            if(pass != null && pass != "" && pass != _student.Pass)
            {
                if (_oldPass.Text != _student.Pass)
                {
                    throw new Exception("Не верен старый пароль!");
                }

                if (pass.Length < 4)
                {
                    throw new Exception("Длина пароля не может быть меньше 4 символов!");
                }

                if (pass != _newPassAgain.Text)
                {
                    throw new Exception("Пароли не совпадают!");
                }

                int strength = PasswordEvaluator.GetStrength(pass);
                if (strength < 2)
                {
                    throw new Exception("Новый пароль слишком слабый!");
                }

                canWrite = true;
            }

            return canWrite;
        }
    }
}