﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.JsonView;
using Mentor.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Mentor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StartTestPage : ContentPage
    {
        Student _student;
        TestBlank _testBlank;
        RestClient _restClient = new RestClient();
        Random _random = new Random();

        public StartTestPage()
        {
            InitializeComponent();
        }

        public StartTestPage(Student student)
        {

            _student = student;

            InitializeComponent();
        }

        private async void StartButton_Clicked(object sender, EventArgs e)
        {
            buttonStart.IsEnabled = false;
            activityIndicatorGrid.IsVisible = true;
            Response response = await _restClient.GetTestBlank(_student);
            if (response.Id > -1)
            {
                _testBlank = (TestBlank) response;
                TestProcessPage page = new TestProcessPage(_testBlank, _student, _random);

                await Navigation.PushModalAsync(page, false);                
            }
            else
            {
                await DisplayAlert("Внимание!", "Тест еще не роздан. Доступ запрещен.", "OK");
            }
            buttonStart.IsEnabled = true;
            activityIndicatorGrid.IsVisible = false;
        }
    }
}