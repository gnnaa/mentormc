﻿//using Android.OS;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mentor.Interfaces;
using Mentor.JsonView;
using Mentor.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.IO;
//using Plugin.DeviceInfo;

namespace Mentor
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AutorizationPage : ContentPage
    {
        RestClient _restClient = new RestClient();
        Student _student;
        AuthorizationRequest _localAuthorizationData;
        MainPage mPage;
        public AutorizationPage()
        {
            InitializeComponent();

            this.Appearing += OnAppearing;
        }

        

        public AutorizationPage(Student student, MainPage mPage)
        {
            _student = student;
            InitializeComponent();
            this.mPage = mPage;
            this.Appearing += OnAppearing;
        }

        private void OnAppearing(Object sender, EventArgs e)
        {
            AuthorizationRequest request = AutorizationSubstitute.GetAutorizationData();
            if(request != null)
            {
                _localAuthorizationData = request;
                entryLogin.Text = request.Login;
                entryPass.Text = request.Pass;
            }            
        }

        private async void buttonEnter_Clicked(object sender, EventArgs e)
        {
            buttonEnter.IsEnabled = false;
            AuthorizationRequest request = new AuthorizationRequest();
            request.Login = entryLogin.Text;
            request.Pass = entryPass.Text;
            activityIndicatorGrid.IsVisible = true;
                        
            Response response = await _restClient.GetStudent(request);

            if (response.Id > -1)
            {
                if( _localAuthorizationData == null                || 
                    request.Login != _localAuthorizationData.Login || 
                    request.Pass != _localAuthorizationData.Pass)
                {
                    bool result = await this.DisplayAlert("Автоподстановка", "Сохранить новые данные авторизации для автоподстановки?", "Да", "Нет");
                    if (result)
                    {
                        AutorizationSubstitute.WriteAutorizationData(request);
                    }
                }
                _student.Set((Student)response);
                mPage.SetTitle(_student.Name);
                labelActivityIndicator.Text = "Синхронизация";

                Synchronizer.FileName = _student.Id.ToString();
                Synchronizer.SharedSec = _student.SharedSec;
                if (Synchronizer.SharedSec == "") Synchronizer.SharedSec = "U7b4X0n5Yd";

                ///Synchronizer synchronizer = new Synchronizer();
                //await synchronizer.Synchronize(_student);

                await Navigation.PopModalAsync(false);
            }
            else
            {
                await DisplayAlert("Вход", response.Message, "OK");
            }
            buttonEnter.IsEnabled = true;
            activityIndicatorGrid.IsVisible = false;
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var result = await this.DisplayAlert("Внимание!", "Вы уверены что хотите закрыть приложение?", "Да", "Нет");
                //if (result) { Process.KillProcess(Process.MyPid()); }
            });

            return true;
        }
    }
}