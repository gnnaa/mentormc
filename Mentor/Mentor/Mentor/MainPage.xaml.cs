﻿using Mentor.JsonView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Mentor
{
    public partial class MainPage : TabbedPage
    {
        RestClient _restClient = new RestClient();
        Student _student;
        TestBlank _testBlank;

        StartTestPage _startTestPage;
        ResultsPage _resultsPage;
        SettingsPage _settingsPage;

        public void SetTitle(string name)
        {
            this.Title = name;
        }

        public MainPage()
        {
            _student = new Student();
            //_student.GenerateTestData();

            AutorizationPage page = new AutorizationPage(_student, this);
            Navigation.PushModalAsync(page);

            InitializeComponent();

            _startTestPage = new StartTestPage(_student);
            _startTestPage.Title = "Тестирование";
            Children.Add(_startTestPage);

            _resultsPage = new ResultsPage(_student);
            _resultsPage.Title = "Результаты";
            Children.Add(_resultsPage);

            _settingsPage = new SettingsPage(_student);
            _settingsPage.Title = "Опции";
            Children.Add(_settingsPage);
        }

        //private async void buttonEnter_Clicked(object sender, EventArgs e)
        //{
        //    
        //}

        private void ClearSelection()
        {

        }

        //private void buttonFinish_Clicked(object sender, EventArgs e)
        //{

        //}
    }
}
