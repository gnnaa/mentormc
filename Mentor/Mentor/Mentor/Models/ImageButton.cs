﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Mentor.Models
{
    public class ImageButton
    {
        private bool _isOn = false;
        private Image _image;
        private string _onPath;
        private string _offPath;

        public ImageButton(Image im, string on, string off)
        {
            _image = im;
            _onPath = on;
            _offPath = off;
            _image.Source = ImageSource.FromResource(_offPath);
        }

        public bool IsOn
        {
            get
            {
                return _isOn;
            }
            set
            {
                if(value == true)
                {
                    if(_isOn == false)
                    {
                        _isOn = true;
                        _image.Source = ImageSource.FromResource(_onPath);
                    }
                }
                else
                {
                    if (_isOn == true)
                    {
                        _isOn = false;
                        _image.Source = ImageSource.FromResource(_offPath);
                    }
                }
            }
        }
    }
}
